package uk.co.hw.ilab.soinn.evaluation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Random;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import uk.co.hw.ilab.soinn.client.SoinnClient;
import uk.co.hw.ilab.soinn.models.SoinnDataset;
import uk.co.hw.ilab.soinn.models.SoinnExample;
import uk.co.hw.ilab.soinn.models.SoinnPrediction;
import uk.co.hw.ilab.soinn.utils.SoinnUtils;

public class Evaluator {

	public static void main(String[] args) throws IOException, ParseException {
		if (args.length != 8) {
			System.err.println("[USAGE] dataset_path outputFile split seed bin_size duplication_factor number_of_iterations modality");
			System.exit(0);
		}
		Path datasetPath = Paths.get(args[0]);
		Path outputFile = Paths.get(args[1]);
		float split = Float.parseFloat(args[2]);
		long seed = Long.parseLong(args[3]);
		int binSize = Integer.parseInt(args[4]);
		int duplicationFactor = Integer.parseInt(args[5]);
		int numberOfIterations = Integer.parseInt(args[6]);
		String modality = args[7];
		Evaluator ism = new Evaluator(datasetPath, outputFile, split, seed, binSize, duplicationFactor, modality);
		for (int i=0; i<numberOfIterations; i++) {
			ism.run();
		}
		
	}
	
	private SoinnDataset sd;
	private Random random;
	private int binSize;
	private float split;
	private double tp = 0;
	private double fp = 0;
	private double fn = 0;
	private String modality;
	
	
	private SoinnClient client;
	private Path outputPath;
	
	public Evaluator(Path datasetPath, Path outputPath, float split, long seed, int binSize, int duplicationFactor, String modality) throws IOException {
		this.split = split;
		this.random = new Random(seed);
		this.binSize = binSize;
		this.outputPath = outputPath;
		this.modality = modality;
		this.client = new SoinnClient();
		this.sd = new SoinnDataset();
		this.sd.populate(datasetPath);
		this.sd.duplicate(duplicationFactor, this.random);
	}
	
	public void run() throws ParseException, IOException {
		this.client.reset();
		if (this.modality.equalsIgnoreCase("split")) {
			this.runRandomSplit();
		} else if (this.modality.equalsIgnoreCase("incr")) {
			this.runIncremental();
		} else if (this.modality.equalsIgnoreCase("incr_test")) {
			this.runIncrementalWithTest();
		} else {
			System.out.println("The modality is not valid. Choose among [split|incr|incr_test].");
		}
	}
	
	public void runRandomSplit() throws ParseException, IOException {
		SoinnDataset[] splits = this.sd.split(this.split, this.random);
		SoinnDataset trainSet = splits[0];
		SoinnDataset testSet = splits[1];
		
		int binIterator = 1;
		JSONObject response = null;
		for (SoinnExample s : trainSet) {
			System.out.println("Learning object: " + s.toString());
			this.client.learnObject(s.getLabel(), s.getInstanceId());
			if (binIterator % this.binSize == 0 || trainSet.size() == binIterator) {
				this.resetPerformances();
				System.out.println("\n\n############### Iteration " + binIterator / this.binSize + " ###############\n\n");
				for (SoinnExample t : testSet) {
					SoinnPrediction sp = null;
					response = this.client.classify(t.getInstanceId(), t.getLabel());
					
					sp = SoinnUtils.getArgmax(SoinnUtils.json2PredictionSortedMap(response)); 
					System.out.println("Classifying object: " + s.toString() + " -> " + sp);
					if (sp == null) {
						fp++;
			        } else {
			        	if (t.getLabel().equalsIgnoreCase(sp.getPrediction())) {
			        		tp++;
			        	} else {
			        		fp++;
			        	}
			        }
					
				}
				System.out.println("\nPrecision:\t" + this.getPrecision() + "\n");
				Files.write(this.outputPath, (this.getPrecision() + "\t").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
			}
			binIterator++;
		}
		Files.write(this.outputPath, ("\n").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
	}
	
	public void runIncremental() throws ParseException, IOException {
		this.sd.shuffle(this.random);
		int binIterator = 1;
		System.out.println("############### Start Bin 0 ###############\n");
		JSONObject response = null;
		SoinnPrediction sp;
		for (SoinnExample s : this.sd) {
			sp = null;
			response = this.client.classify(s.getInstanceId(), s.getLabel());
			sp = SoinnUtils.getArgmax(SoinnUtils.json2PredictionSortedMap(response));
			System.out.println("Classifying object: " + s.toString() + " -> " + sp);
			if (sp == null) {
				System.out.println("Learning object: " + s.toString());
	        	this.client.learnObject(s.getLabel(), s.getInstanceId());
	        	fp++;
	        } else {
	        	if (s.getLabel().equalsIgnoreCase(sp.getPrediction())) {
	        		tp++;
	        	} else {
	        		fp++;
	        		this.client.learnObject(s.getLabel(), s.getInstanceId());
	        		System.out.println("Learning object: " + s.toString());
	        	}
	        }
			if (binIterator % this.binSize == 0 || this.sd.size() == binIterator) {
				System.out.println("\nPrecision:\t" + this.getPrecision() + "\n");
				System.out.println("################# End Bin #################\n\n");
				Files.write(this.outputPath, (this.getPrecision() + "\t").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				resetPerformances();
				System.out.println("############### Start Bin " + (binIterator) / this.binSize + " ###############\n");
			}
			binIterator++;
		}
		Files.write(this.outputPath, ("\n").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
	}
	
	public void runIncrementalWithTest() throws ParseException, IOException {
		this.sd.shuffle(this.random);
		SoinnDataset testSet = new SoinnDataset();
		testSet.clear();
		int binIterator = 1;
		System.out.println("############### Start Bin 0 ###############\n");
		JSONObject response = null;
		SoinnPrediction sp = null;
		for (SoinnExample s : this.sd) {
			sp = null;
			response = this.client.classify(s.getInstanceId(), s.getLabel());
			sp = SoinnUtils.getArgmax(SoinnUtils.json2PredictionSortedMap(response));
			System.out.println("Classifying object: " + s.toString() + " -> " + sp);
			if (sp == null) {
	        	this.client.learnObject(s.getLabel(), s.getInstanceId());
	        	System.out.println("Learning object: " + s.toString());
	        } else {
	        	if (!s.getLabel().equalsIgnoreCase(sp.getPrediction())) {
	        		this.client.learnObject(s.getLabel(), s.getInstanceId());
	        		System.out.println("Learning object: " + s.toString());
	        	}
	        }
			testSet.add(s);
			if (binIterator % this.binSize == 0 || this.sd.size() == binIterator) {
				for (SoinnExample t : testSet) {
					sp = null;
					response = this.client.classify(t.getInstanceId(), t.getLabel());
					sp = SoinnUtils.getArgmax(SoinnUtils.json2PredictionSortedMap(response));
					if (sp == null) {
						fp++; 
			        } else {
			        	if (t.getLabel().equalsIgnoreCase(sp.getPrediction())) {
			        		tp++;
			        	} else {
			        		fp++;
			        	}
			        }
				}
				System.out.println("\nPrecision:\t" + this.getPrecision() + "\n");
				System.out.println("################# End Bin #################\n\n");
				Files.write(this.outputPath, (this.getPrecision() + "\t").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				resetPerformances();
				testSet.clear();
				System.out.println("############### Start Bin " + (binIterator) / this.binSize + " ###############\n");
			}
			binIterator++;
		}
		Files.write(this.outputPath, ("\n").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
	}
	
	public SoinnClient getClient() {
		return client;
	}

	public void setClient(SoinnClient client) {
		this.client = client;
	}

	public double getPrecision() {
		if (tp == 0 && fp == 0)
			return 0;
		return tp / (tp+fp);
	}
	
	public double getRecall() {
		if (tp == 0 && fn == 0)
			return 0;
		return tp / (tp+fn);
	}
	
	public double getFMeasure() {
		if (this.tp == 0 && this.fn == 0)
			return 0;
		return ((2 * this.tp) / ((2 * this.tp) + this.fp + this.fn));
	}
	
	public void resetPerformances() {
		this.tp = 0;
		this.fp = 0;
		this.fn = 0;
	}
}
