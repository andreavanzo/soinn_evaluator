package uk.co.hw.ilab.soinn.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import uk.co.hw.ilab.soinn.models.SoinnPrediction;


public class SoinnUtils {

	@SuppressWarnings("unchecked")
	public static Map<String, Double> json2PredictionSortedMap(JSONObject json) {
		Map<String, Double> predictMap = new HashMap<String, Double>();
		JSONArray predictions = (JSONArray) json.get("predictions");
		if (predictions == null)
			return null;
		Iterator<JSONObject> iterator = predictions.iterator();
		while (iterator.hasNext()) {
			JSONObject prediction = iterator.next();
			predictMap.put((String) prediction.get("label"), Double.parseDouble((String) prediction.get("score")));
		}
		Map<String, Double> sortedMap = MapToolKit.getInstance().sortByComparator(predictMap, true);
		return sortedMap;
	}

	public static SoinnPrediction getArgmax(Map<String, Double> predictions) {
		if (predictions == null || predictions.isEmpty())
			return null;
		Map.Entry<String, Double> entry = predictions.entrySet().iterator().next();
		String key = entry.getKey();
		Double value = entry.getValue();
		return new SoinnPrediction(key, value);
	}

	public static boolean json2Boolean(JSONObject json) {
		boolean toReturn = false;
		toReturn = Boolean.parseBoolean((String) json.get("result"));
		return toReturn;
	}
}
