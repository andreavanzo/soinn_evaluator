package uk.co.hw.ilab.soinn.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MapToolKit {
	private static MapToolKit instance;	
	
	private MapToolKit(){}
	 
	public static MapToolKit getInstance() 
    {
  		if (instance == null)
  			instance = new MapToolKit();
      return instance;
    }	
	
	public Map<String, Double> sortByComparator(Map<String, Double> unsortMap, boolean isDescend) {

		// Convert Map to List
		List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());
	
		Comparator<Map.Entry<String, Double>> comparator = new Comparator<Map.Entry<String, Double>>() {
			public int compare(Map.Entry<String, Double> o1,
                    Map.Entry<String, Double> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		};
		
		if(isDescend)
			comparator = Collections.reverseOrder(comparator);
		
		Collections.sort(list,comparator);
		
		// Convert sorted map back to a Map
		Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
				
		for (Iterator<Map.Entry<String, Double>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Double> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
				
		return sortedMap;
	}
	
}
