package uk.co.hw.ilab.soinn.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SoinnClient {
	
	private static final String BASE_URL = "http://localhost:";
	private static final String PORT = "9001";
	private static final String INPUT_SIGNAL = "inputSignal";
	private static final String CLASSIFY = "classify";
	private static final String RESET = "reset";
	private static final String INIT = "initialization";
	
	public SoinnClient() {
		this.initialization();
		this.reset();
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject learnObject(String instanceLabel, String instanceId) {
		JSONObject params = new JSONObject();
		params.put("instance_label", instanceLabel);
		params.put("instance_id", instanceId);
		try {
			params = postRequest(INPUT_SIGNAL, params);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return params;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject classify(String instanceId, String instanceLabel) {
		JSONObject params = new JSONObject();
		params.put("instance_label", instanceLabel);
		params.put("instance_id", instanceId);
		try {
			params = postRequest(CLASSIFY, params);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return params;
	}
	
	public void reset() {
		JSONObject params = new JSONObject();
		try {
			postRequest(RESET, params);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void initialization() {
		JSONObject params = new JSONObject();
		try {
			postRequest(INIT, params);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private JSONObject postRequest(String url, JSONObject params)
			throws MalformedURLException, IOException, ProtocolException, UnsupportedEncodingException, ParseException {
		URL u = new URL(BASE_URL + PORT + "/" + url);
		
		HttpURLConnection connection = (HttpURLConnection) u.openConnection();
		
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");
		
		
		OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
		osw.write(params.toJSONString());
		osw.flush();
		
		int responseCode =  connection.getResponseCode();
		StringBuilder sb = new StringBuilder();
		JSONObject reply = null;
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			
			JSONParser json = new JSONParser();
			reply = (JSONObject) json.parse(sb.toString());
		}
		return reply;
	}

}
