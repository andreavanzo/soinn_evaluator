package uk.co.hw.ilab.soinn.models;

public class SoinnPrediction {
	
	private String prediction;
	private Double confidenceScore;
	
	public SoinnPrediction() {
		
	}
	
	public SoinnPrediction(String prediction, Double confidenceScore) {
		this.prediction = prediction;
		this.confidenceScore = confidenceScore;
	}
	
	public String getPrediction() {
		return prediction;
	}
	public void setPrediction(String prediction) {
		this.prediction = prediction;
	}
	public Double getConfidenceScore() {
		return confidenceScore;
	}
	public void setConfidenceScore(Double confidenceScore) {
		this.confidenceScore = confidenceScore;
	}
	
	public String toString() {
		return "PredictedLabel: " + this.prediction + "\tConfidenceScore: " + this.confidenceScore;
	}

}
