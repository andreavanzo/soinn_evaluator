package uk.co.hw.ilab.soinn.models;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Random;

public class SoinnDataset extends ArrayList<SoinnExample> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7943314359913895419L;

	private int counter = 0;

	/**
	 * Method tha allows to populate the {@link SoinnDataset}, starting from the path of the dataset.
	 * @param path The path of the dataset folder.
	 * @throws IOException
	 */
	public void populate(Path path) throws IOException {
		System.out.print("Start populating");
		long start = System.currentTimeMillis();
		counter = 0;
		extractDataset(path);
		long end = System.currentTimeMillis();
		System.out.println("done [" + (end - start) / 1000 + " sec].");
	}

	private void extractDataset(Path path) throws IOException {
		String dir = "";
		String instance = "";
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
			for (Path entry : stream) {
				if (Files.isDirectory(entry)) {
					if (entry.getFileName().toString().matches(".*_[0-9]+")) {
						if (counter % 20 == 0)
							System.out.print(".");
						counter++;
						dir = entry.getParent().getFileName().toString();
						instance = entry.getFileName().toString();
						this.add(new SoinnExample(dir, instance));
					}
					extractDataset(entry);
				}
			}
		}
	}
	
	public void duplicate(int duplicationFactor, Random random) {
		if (duplicationFactor <= 1)
			return;
		SoinnDataset tempDataset = new SoinnDataset();
		for (int i=1; i<duplicationFactor; i++) {
			tempDataset.addAll(this);
		}
		tempDataset.shuffle(random);
		this.addAll(tempDataset);
	}
	
	/**
	 * This method allows to shuffle the dataset.
	 * 
	 * @param random the {@link Random} Object, enabling for a pseudo-random shuffling 
	 */
	public void shuffle(Random random) {
		Collections.shuffle(this, random);
	}
	

	/**
	 * The method perform a random nfold split
	 * 
	 * @param numberOfFolds
	 *            the number of folds
	 * @param random
	 *            the {@link Random} object to shuffle the dataset
	 * @return an array of {@link DatasetFolderStructure} folds
	 */
	public SoinnDataset[] split(int numberOfFolds, Random random) {
		SoinnDataset[] sd = new SoinnDataset[numberOfFolds];
		for (int i = 0; i < sd.length; i++) {
			sd[i] = new SoinnDataset();
		}
		int i = 0;
		Collections.shuffle(this, random);
		for (SoinnExample example : this) {
			sd[i % numberOfFolds].add(example);
			i++;
		}
		for (int j = 0; j < sd.length; j++) {
			System.out.println("Instances of fold " + j + ": " + sd[j].size());
		}
		return sd;
	}
	
	/**
	 * The method perform a random nfold split
	 * 
	 * @param numberOfFolds
	 *            the number of folds
	 * @param random
	 *            the {@link Random} object to shuffle the dataset
	 * @return an array of {@link DatasetFolderStructure} folds
	 */
	public SoinnDataset[] splitLabelDistributionInvariant(int numberOfFolds, Random random) {
		
		LinkedHashMap<String, ArrayList<SoinnExample>> datasetPerClass = new LinkedHashMap<String, ArrayList<SoinnExample>>();
		SoinnDataset[] sd = new SoinnDataset[numberOfFolds];
		for (int i = 0; i < sd.length; i++) {
			sd[i] = new SoinnDataset();
		}
		for (SoinnExample x : this) {
			if (!datasetPerClass.containsKey(x.getLabel())) {
				datasetPerClass.put(x.getLabel(), new ArrayList<SoinnExample>());
			}
			datasetPerClass.get(x.getLabel()).add(x);
		}
		int i = 0;
		for (String predicate : datasetPerClass.keySet()) {
			Collections.shuffle(datasetPerClass.get(predicate), random);
			for (SoinnExample x : datasetPerClass.get(predicate)) {
				sd[i % numberOfFolds].add(x);
				i++;
			}
		}

		for (int j = 0; j < sd.length; j++) {
			System.out.println("Instances of split " + j + ": " + sd[j].size());
		}
		
		return sd;
	}

	/**
	 * The method performs a random split.
	 * 
	 * @param trainPerc
	 *            the percentage of data of the training set
	 * @param random
	 *            the {@link Random} object to shuffle the dataset
	 * @return an array of {@link DatasetFolderStructure} objects
	 */
	public SoinnDataset[] split(float trainPerc, Random random) {
		SoinnDataset[] sd = new SoinnDataset[2];
		for (int i = 0; i < sd.length; i++) {
			sd[i] = new SoinnDataset();
		}
		Collections.shuffle(this, random);
		for (SoinnExample instance : this) {
			if (random.nextFloat() < trainPerc) {
				sd[0].add(instance);
			} else {
				sd[1].add(instance);
			}
		}
		for (int j = 0; j < sd.length; j++) {
			System.out.println("Instances of split " + j + ": " + sd[j].size());
		}

		return sd;
	}

	/**
	 * The method performs a random split maintaing the distribution of the
	 * classes invariant among the two sets.
	 * 
	 * @param trainPerc
	 *            the percentage of data of the training set
	 * @param random
	 *            the {@link Random} object to shuffle the dataset
	 * @return an array of {@link DatasetFolderStructure} objects
	 */
	public SoinnDataset[] splitLabelDistributionInvariant(float trainPerc, Random random) {
		LinkedHashMap<String, ArrayList<SoinnExample>> datasetPerClass = new LinkedHashMap<String, ArrayList<SoinnExample>>();
		SoinnDataset[] sd = new SoinnDataset[2];
		for (int i = 0; i < sd.length; i++) {
			sd[i] = new SoinnDataset();
		}
		for (SoinnExample x : this) {
			if (!datasetPerClass.containsKey(x.getLabel())) {
				datasetPerClass.put(x.getLabel(), new ArrayList<SoinnExample>());
			}
			datasetPerClass.get(x.getLabel()).add(x);
		}
		for (String predicate : datasetPerClass.keySet()) {
			Collections.shuffle(datasetPerClass.get(predicate), random);
			for (SoinnExample x : datasetPerClass.get(predicate)) {
				if (random.nextFloat() < trainPerc) {
					sd[0].add(x);
				} else {
					sd[1].add(x);
				}
			}
		}

		for (int j = 0; j < sd.length; j++) {
			System.out.println("Instances of split " + j + ": " + sd[j].size());
		}

		return sd;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (SoinnExample se : this) {
			sb.append(se.getLabel() + "\t" + se.getInstanceId() + "\n");
		}
		return sb.toString();
	}

}
