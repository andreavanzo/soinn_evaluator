package uk.co.hw.ilab.soinn.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SoinnExample {
	
	private String label;
	private ArrayList<SoinnPrediction> predictedLabels;
	private String instanceId;
	
	public SoinnExample() {
		this.predictedLabels = new ArrayList<SoinnPrediction>();
	}
	
	public SoinnExample(String label, String instanceId) {
		super();
		this.label = label;
		this.instanceId = instanceId;
		this.predictedLabels = new ArrayList<SoinnPrediction>();
	}
	
	public boolean isPredictionCorrect() {
		return this.getPredictedLabels().get(0).getPrediction().equalsIgnoreCase(this.getLabel());
	}
	
	public void clearPredictions() {
		this.predictedLabels.clear();
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ArrayList<SoinnPrediction> getPredictedLabels() {
		return predictedLabels;
	}

	public void addPredictedLabel(SoinnPrediction sp) {
		this.predictedLabels.add(sp);
		Collections.sort(this.predictedLabels, new Comparator<SoinnPrediction>() {
			@Override
			public int compare(SoinnPrediction o1, SoinnPrediction o2) {
				if (o1.getConfidenceScore() > o2.getConfidenceScore())
					return -1;
				else if (o1.getConfidenceScore() < o2.getConfidenceScore())
					return 1;
				else
					return 0;
			}
		});
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.instanceId);
		sb.append("[");
		sb.append(this.label);
		sb.append("]");
		return sb.toString();
	}
}
